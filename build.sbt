ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-unchecked",
  "-feature",
  "-language:higherKinds",
  "-language:postfixOps",
  "-Xfatal-warnings"
)

lazy val root = (project in file("."))
  .settings(
    name := "assignment"
  )

ThisBuild / libraryDependencies ++= Seq(
  "joda-time" % "joda-time" % "2.10.14",
  "org.scalatest" %% "scalatest" % "3.2.12" % "test"
)

