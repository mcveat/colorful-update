package assignment

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.must.Matchers

class JobFeasibilityCheckerSpec extends AnyFreeSpec with Matchers {
  "Job feasibility checker" - {
    val checker = RecursiveJobFeasibilityChecker
    "should treat empty job list as feasible" in {
      checker.feasible(input = List.empty) must be(true)
    }
    "should treat acyclic graph as feasible" in {
      checker.feasible(List((5, 3), (7, 3), (3, 4))) must be(true)
    }
    "should treat cycle graph as not feasible" in {
      checker.feasible(List((1, 2), (2, 3), (3, 1))) must be(false)
    }
    "should treat graph with cycle as not feasible" in {
      checker.feasible(List((0, 1), (1, 2), (2, 3), (3, 1), (3, 4))) must be(false)
    }
  }
}
