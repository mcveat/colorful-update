package assignment

import assignment.ScheduleGenerator.{Schedule, parseDate}
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.must.Matchers

class ScheduleGenerationSpec extends AnyFreeSpec with Matchers {
  "Schedule generator" - {
    val generator = FixedScheduleGenerator
    "when given end day under 30 days from start date" - {
      val startDate = "01/03/2022"
      val input = (startDate, "01/30/2022")
      "should generate a schedule with only start date" in {
        generator.scheduleFor(input) must be(Schedule(Seq(parseDate(startDate))))
      }
    }
    "when given start and end date exactly 30 days apart" - {
      val startDate = "01/03/2022"
      val endDate = "02/02/2022"
      val input = (startDate, endDate)
      "should generate a schedule with start and end date" in {
        generator.scheduleFor(input) must be(Schedule(Seq(startDate, endDate).map(parseDate)))
      }
    }
    "when given end date 2 months ahead" - {
      val input = ("01/03/2022", "03/04/2022")
      "should generate a schedule that has 3 dates" in {
        val expected = Schedule(Seq("01/03/2022", "02/2/2022", "03/04/2022").map(parseDate))
        generator.scheduleFor(input) must be(expected)
      }
    }
    "should adjust the date to next monday" - {
      val saturday :: sunday :: nextMondayStr :: Nil = List("01/01/2022", "01/02/2022", "01/03/2022")
      val endDate = "02/01/2022"
      val nextMonday = parseDate(nextMondayStr)
      s"if it falls on saturday, like $saturday" in {
        val input = (saturday, endDate)
        generator.scheduleFor(input).dates.head must be(nextMonday)
      }
      s"if it falls on sunday, like $sunday" in {
        val input = (sunday, endDate)
        generator.scheduleFor(input).dates.head must be(nextMonday)
      }
      s"if it falls on sunday, like 04/03/2022" in {
        val input = ("01/03/2022", "04/15/2022")
        generator.scheduleFor(input).dates.last must be(parseDate("04/04/2022"))
      }
      s"but not if that puts it past the end date" in {
        val input = ("01/03/2022", "04/03/2022")
        generator.scheduleFor(input).dates.last must be(parseDate("03/04/2022"))
      }
    }
    "can return empty schedule, if it starts on saturday and ends on sunday" in {
      val input = ("01/01/2022", "01/02/2022")
      generator.scheduleFor(input) must be(Schedule.empty)
    }
  }
}
