package assignment

import assignment.ScheduleGenerator.{Input, Schedule, parseDate}
import org.joda.time.DateTimeConstants.{SATURDAY, SUNDAY}
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat

trait ScheduleGeneratorLike {
  def scheduleFor(input: Input): Schedule
}

object FixedScheduleGenerator extends ScheduleGeneratorLike {
  private val Interval = 30

  override def scheduleFor(input: (String, String)): Schedule = {
    val (startDateStr, endDateStr) = input
    val startDate :: endDate :: Nil = List(startDateStr, endDateStr).map(parseDate)
    lazy val dates: LazyList[LocalDate]  = startDate #:: dates.map(_.plusDays(Interval))
    val datesOnWeekdays = dates.map(skipWeekend)
    Schedule(datesOnWeekdays.takeWhile(!_.isAfter(endDate)))
  }

  private def skipWeekend(date: LocalDate): LocalDate = date.getDayOfWeek match {
    case SATURDAY => date.plusDays(2)
    case SUNDAY => date.plusDays(1)
    case _ => date
  }
}

object ScheduleGenerator {
  type Input = (String, String)
  case class Schedule(dates: Seq[LocalDate]) extends AnyVal
  object Schedule {
    val empty: Schedule = Schedule(dates = Seq.empty)
  }
  private val dateFormat = DateTimeFormat.forPattern("MM/dd/yyyy")
  private[assignment] def parseDate(s: String): LocalDate = LocalDate.parse(s, dateFormat)
}