package assignment

import assignment.JobFeasibilityChecker.Input

import scala.annotation.tailrec

trait JobFeasibilityCheckerLike {
  def feasible(input: Input): Boolean
}

object RecursiveJobFeasibilityChecker extends JobFeasibilityCheckerLike {
  private case class Job(value: Int) extends AnyVal
  private case class Graph(nodes: Map[Job, Set[Job]] = Map.empty) {
    def add(job: Job): Graph = Graph(nodes + (job -> dependenciesOf(job)))
    def add(job: Job, dependency: Job): Graph = Graph(nodes + (job -> (dependenciesOf(job) + dependency)))
    private def dependenciesOf(job: Job) = nodes.getOrElse(job, Set.empty)
  }

  override def feasible(input: Input): Boolean = {
    val graph = input.foldLeft(Graph()) { case (graph, (jobValue, dependencyValue)) =>
      val job :: dependency :: Nil = List(jobValue, dependencyValue).map(Job.apply)
      graph.add(job, dependency).add(dependency)
    }
    acyclic(graph)
  }

  @tailrec
  private def acyclic(graph: Graph): Boolean = {
    val (independentIterable, rest) = graph.nodes.partitionMap { case (job, dependency) =>
      if (dependency.isEmpty) Left(job) else Right(job -> dependency)
    }
    if (rest.isEmpty) return true
    val independent = independentIterable.toSet
    if (independent.isEmpty) return false
    val trimmed = rest.toMap.view.mapValues(_.diff(independent)).toMap
    acyclic(Graph(trimmed))
  }
}

object JobFeasibilityChecker {
  type JobWithDependency = (Int, Int)
  type Input = List[JobWithDependency]
}
